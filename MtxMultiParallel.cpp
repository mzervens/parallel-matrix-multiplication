#include <iostream>
#include <ppl.h>
using namespace std;
using namespace concurrency;

class matrix
{
private:
	int xLen;
	int yLen;

public:
	double ** mtx;

	matrix(int n, int m)
	{
		mtx = new double *[n];
		for (int i = 0; i < n; i++)
		{
			mtx[i] = new double[m];
		}

		yLen = n;
		xLen = m;
	}

	matrix()
	{
	}

	~matrix()
	{
		for (int i = 0; i < yLen; i++)
		{
			delete mtx[i];
		}

		delete mtx;
	}

	int xLength()
	{
		return xLen;
	}

	int yLength()
	{
		return yLen;
	}

	bool setCell(int col, int row, double v)
	{
		mtx[row][col] = v;
		return true;
	}

	bool setRow(int row, double data[])
	{
		for (int i = 0; i < xLen; i++)
		{
			mtx[row][i] = data[i];
		}
		return true;
	}


	double getCell(int col, int row)
	{
		return mtx[row][col];

	}

	bool transposeQuadratic()
	{
		if (xLen != yLen)
		{
			return false;
		}

		double tmpCell;

		for (int i = 0; i < yLen; i++)
		{
			for (int n = i + 1; n < xLen; n++)
			{
				tmpCell = mtx[i][n];
				mtx[i][n] = mtx[n][i];
				mtx[n][i] = tmpCell;
			}
		}

		return true;

	}

	bool isQuadratic()
	{
		if (xLen == yLen)
		{
			return true;
		}
		return false;
	}

	matrix * multiply(matrix * m)
	{
		if (yLen != m->xLength())
		{
			return new matrix();
		}

		matrix * output = new matrix(yLen, m->xLength());
		if (m->isQuadratic())
		{
			m->transposeQuadratic();
			parallel_for(0, yLen, 1, [&](int i)
			{
				for (int n = 0; n < m->xLength(); n++)
				{
					output->mtx[i][n] = 0;
					for (int s = 0; s < xLen; s++)
					{
						output->mtx[i][n] += mtx[i][s] * m->mtx[n][s];
					}
				}
			});

			m->transposeQuadratic();
			return output;
		}
		// T�l�k seko metodes zars, kas atbilst otraj� optimiz�cij� veiktajam kodam
		double localCell = 0;
		for (int i = 0; i < yLen; i++)
		{
			for (int n = 0; n < m->xLength(); n++)
			{
				localCell = 0;
				//output->mtx[i][n] = 0;
				//output->setCell(n, i, 0);
				for (int s = 0; s < xLen; s++)
				{
					localCell = localCell + mtx[i][s] * m->mtx[s][n];
					//output->mtx[i][n] = output->mtx[i][n] + mtx[i][s] * m->mtx[s][n];
					//output->setCell(n, i, output->getCell(n, i) + (mtx[i][s] * m->getCell(n, s)));
				}
				output->mtx[i][n] = localCell;

			}

		}

		return output;
	}

	void print()
	{
		for (int i = 0; i < yLen; i++)
		{
			for (int n = 0; n < xLen; n++)
			{
				cout << mtx[i][n] << " ";
			}

			cout << "\n";

		}

	}

};