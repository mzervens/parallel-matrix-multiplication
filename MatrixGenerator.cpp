#include <random>;
#include "dType.cpp";
using namespace std;

template <typename T>
T matrixGenerator(T from, T to, dType t)
{
	random_device rd;
	mt19937 merseneTwister(rd());
	
	if (t == dType::INTEGER32)
	{
		uniform_int_distribution<int> dist(from, to);
		return dist(merseneTwister);

	}
	else
	{
		uniform_real_distribution<double> dist(from, to);
		return dist(merseneTwister);
	}


}