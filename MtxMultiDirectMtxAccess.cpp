#include <iostream>
using namespace std;

class matrix
{
private:
	int xLen;
	int yLen;

public:
	double ** mtx;

	matrix(int n, int m)
	{
		mtx = new double *[n];
		for (int i = 0; i < n; i++)
		{
			mtx[i] = new double[m];
		}

		yLen = n;
		xLen = m;
	}

	matrix()
	{
	}

	~matrix()
	{
		for (int i = 0; i < yLen; i++)
		{
			delete mtx[i];
		}

		delete mtx;
	}

	int xLength()
	{
		return xLen;
	}

	int yLength()
	{
		return yLen;
	}

	bool setCell(int col, int row, double v)
	{
		mtx[row][col] = v;
		return true;
	}

	bool setRow(int row, double data[])
	{
		for (int i = 0; i < xLen; i++)
		{
			mtx[row][i] = data[i];
		}
		return true;
	}


	double getCell(int col, int row)
	{
		return mtx[row][col];

	}

	matrix * multiply(matrix * m)
	{
		if (yLen != m->xLength())
		{
			return new matrix();
		}

		matrix * output = new matrix(yLen, m->xLength());

		for (int i = 0; i < yLen; i++)
		{
			for (int n = 0; n < m->xLength(); n++)
			{
				output->mtx[i][n] = 0;
				//output->setCell(n, i, 0);
				for (int s = 0; s < xLen; s++)
				{
					output->mtx[i][n] = output->mtx[i][n] + mtx[i][s] * m->mtx[s][n];
					//output->setCell(n, i, output->getCell(n, i) + (mtx[i][s] * m->getCell(n, s)));
				}

			}

		}

		return output;
	}

	void print()
	{
		for (int i = 0; i < yLen; i++)
		{
			for (int n = 0; n < xLen; n++)
			{
				cout << mtx[i][n] << " ";
			}

			cout << "\n";

		}

	}

};