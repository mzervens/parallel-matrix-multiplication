#include <fstream>
#include <iostream>
#include <string>
#include "dType.cpp"
#include "MatrixGenerator.cpp"
#include "Timer.cpp"
//#include "MatrixMultiplication.cpp"
//#include "MtxMultiDirectMtxAccess.cpp"
//#include "MtxMulti2Opt.cpp"
#include "MtxMultiParallel.cpp"
using namespace std;

void generateMatrix(int from, int to, int size, dType t, string fName)
{
	fstream f;
	f.open(fName.c_str(), fstream::out);

	//auto min = from;
	//auto max = to;
	double min;
	double max;
	
	if (t == dType::DOUBLE32)
	{
		min = (double) from;
		max = (double) to;
	}

	f << size << "\n";
	for (int i = 0; i < size; i++)
	{
		for (int n = 0; n < size; n++)
		{
			f << matrixGenerator(min, max, t) << " ";

		}
		f << "\n";

	}

	f.close(); 

}

matrix * load(string fName)
{
	char space;
	int size;
	int val;
	matrix * m;
	fstream f;
	f.open(fName.c_str(), fstream::in);

	f >> size;

	m = new matrix(size, size);

	for (int i = 0; i < size; i++)
	{
		for (int n = 0; n < size; n++)
		{
			f >> val;
			m->setCell(i, n, val);

		}

	}


	f.close();


	return m;
}


int main()
{
	//int i;
	//Timer t;
	matrix * a;
	matrix * b;
	matrix * c;

	//generateMatrix(-15, 15, 1000, dType::DOUBLE32, "1000x1000_sdouble.txt");
	//return 0;

	a = load("700x700_sdouble.txt");
	b = load("700x700_sdouble.txt");

	c = b->multiply(a);
	//c->print();


	
	delete a;
	delete b;
	delete c;

	//cout << t.elapsed();
	//cin >> i;
	return 0;
}